$(document).ready(function () {
    // Require the request module
    AP.require(['request', 'messages'], function(request, messages) {
        // store current project info for links and JQL later
        var hostOrigin = AP._data.options.hostOrigin;
        var projectKey = AP._data.options.productContext["project.key"]

        // read Service Desk requests via the Jira REST API
        //  - this allows searching across all users;
        //    /rest/servicedeskapi/request only returns requests that the current user is involved in

        // entry point for searching for issues
        function _searchIssues(resultsStartAt, maxResultsPerRequest, daysToSearch, reporterRequestCounts, reporterDetails) {
            $('#main-content-table').empty();
            $('#refresh-button').attr('disabled', 'disabled');
            $('<span>').addClass('loading').text('Loading...').appendTo('#main-content-table');

            _searchIssuesRecursive(resultsStartAt, maxResultsPerRequest, daysToSearch, reporterRequestCounts, reporterDetails)
        }

        // this function is called recursively to deal with a chain of asynchronous API calls
        function _searchIssuesRecursive(resultsStartAt, maxResultsPerRequest, daysToSearch, reporterRequestCounts, reporterDetails) {
            // search parameters
            //  - TODO: issue types are hard-coded, should be looked up, and could be used as another search parameter
            var searchParams = {
                jql : 'project = ' + projectKey + ' AND issueType IN (Task, Story)',
                startAt: resultsStartAt,
                maxResults: maxResultsPerRequest
            };
            if (daysToSearch) {
                searchParams.jql += ' AND created > -' + daysToSearch + 'd';
            }
            request({
                url: '/rest/api/2/search',
                type: 'POST',
                data: JSON.stringify(searchParams),
                contentType: 'application/json',
     
                success: function (response) {
                    // parse the response JSON
                    var json = JSON.parse(response);

                    // count up the issues for each reporter
                    for (var index = 0; index < json.issues.length; index++) {
                        var issue = json.issues[index];

                        var reporterID = issue.fields.creator.accountId; //reporter.accountId;
                        if (!reporterRequestCounts[reporterID]) {
                            reporterRequestCounts[reporterID] = 1;
                            reporterDetails[reporterID] = issue.fields.creator;
                        } else {
                            reporterRequestCounts[reporterID]++;
                        }
                    }

                    // see if there are more results to read
                    if (json.startAt + json.issues.length < json.total) {
                        _searchIssues(json.startAt + json.issues.length, json.maxResults, daysToSearch, reporterRequestCounts, reporterDetails);
                    } else {
                        _displayIssues(reporterRequestCounts, reporterDetails, daysToSearch);
                    }
                },
                error: function (err) {
                    $('<div>').addClass('aui-message').addClass('aui-message-error').append(
                        $('<p>').addClass('title').append(
                            $('<span>').addClass('aui-icon').addClass('icon-error'),
                            $('<strong>').text('An error occurred!')
                        ),
                        $('<p>').text(err.status + ' ' + err.statusText)
                    ).appendTo('#main-content');
                }
            });
        }

        // display the issue counts as a table
        function _displayIssues(reporterRequestCounts, reporterDetails, daysToSearch) {
            $('#main-content-table').empty();
            $('#refresh-button').removeAttr('disabled');

            // convert the input into an array for $.map()
            //  - sorted in descending order of #requests
            var reportersArray = [];
            for (var reporterID in reporterRequestCounts) {
                reportersArray.push({ reporter: reporterDetails[reporterID], requestCount: reporterRequestCounts[reporterID] });
            }
            reportersArray.sort(function(item1, item2) {
                return (item2.requestCount - item1.requestCount);
            });

            if (reportersArray.length == 0) {
                // Show a link to the Customer Portal
                $('<div>').addClass('aui-message').append(
                    $('<p>').addClass('title').append(
                        $('<span>').addClass('aui-icon').addClass('icon-info'),
                        $('<strong>').text("There are no requests in this project.")
                    ),
                    $('<p>').append(
                        $('<span>').text("Visit the "),
                        $('<a>').attr('href', '/servicedesk/customer/portals')
                                .attr('target', '_blank')
                                .text('Customer Portal'),
                        $('<span>').text(" to create some.")
                    )
                ).appendTo('#main-content-table');
                return;
            }

            // display the users in a table
            var searchJQL = 'project = ' + projectKey + ' AND issueType IN (Task, Story)';
            if (daysToSearch) {
                searchJQL += ' AND created > -' + daysToSearch + 'd';
            }
            $('<table>').addClass('aui').addClass('top-reporters-table').append(
                $('<thead>').append(
                    $('<tr>').append(
                        $('<th>').text('Reporter'),
                        $('<th>').text('#Requests')
                    )
                ),
                $('<tbody>').append(
                    $.map(reportersArray, function(reporterNode) {
                        // map each reporter to a table row, linking to profile and issues reported
                        return $('<tr>').append(
                            $('<td>').append(
                                $('<a>').attr('href', hostOrigin + '/secure/ViewProfile.jspa?name=' + reporterNode.reporter.name)
                                        .attr('title', 'View profile in a new tab')
                                        .attr('target', '_blank')
                                        .text(reporterNode.reporter.displayName)
                            ),
                            $('<td>').append(
                                $('<a>').attr('href', encodeURI(hostOrigin + '/issues/?jql=reporter = ' + reporterNode.reporter.name + ' AND ' + searchJQL))
                                        .attr('title', 'View these issues in a new tab')
                                        .attr('target', '_blank')
                                        .text(reporterNode.requestCount)
                            )
                        );
                    })
                )
            ).appendTo('#main-content-table');
        }

        // this is written as its own function so that it can be invoked without reloading the page
        function _refreshData() {
            // keep request counts per reporters
            var reporterRequestCounts = {};
    
            // keep reporter details by ID for look-up later
            var reporterDetails = {};

            // see how many days we need to search through
            var daysToSearch = $('#daysToSearch').val();

            _searchIssues(0, 50, daysToSearch, reporterRequestCounts, reporterDetails);
        }

        $('#refresh-button').click(function() {
            _refreshData();
        });

        // go!
        _refreshData();
   });
});
